package progs;
import entites.Exploitation;
import java.util.Scanner;
import entites.Parcelle;

public class P01 {

    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in);
        
        
        // NB: Pour vos tests, les id d'exploitation du jeu d'essais sont exp001 et exp002
        
        System.out.println("Identifiant de l'exploitation? ");
        
        String identExploit = clavier.next();
                
        Exploitation exploitation=  data.Dao.getLExploitation(identExploit);
        
        System.out.printf("\n Exploitant: %-25s \n", exploitation.getNomExploitant());
        System.out.printf("\n Adresse: %-25s \n", exploitation.getAdrExploitant());
        System.out.printf("\n Surface totale cultivee: %-25s \n", exploitation.surfaceTotaleDesParcelles()+"\n");
        
        for(Parcelle p : exploitation.getLesParcelles()){
            System.out.println("Surface de la parcelle :"+p.getSurface());
            System.out.println("Espece Cultivée :"+p.getlEspeceCultivee().getNomEsp());
            System.out.println("Date Semis :"+p.getDateSemis());
            System.out.println("Date prevue :"+p.getDateRecoltePrevue()+"\n");
        }
    }
}






