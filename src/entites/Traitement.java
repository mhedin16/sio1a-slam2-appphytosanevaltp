package entites;



public abstract class Traitement {

    private String   idTrait;
   
    private Produit  leProduit; 
    private Parcelle laParcelle;

    public Traitement() {}

    public Traitement(String idTrait, Produit leProduit, Parcelle laParcelle) {
        
        this.idTrait     = idTrait;
        this.leProduit   = leProduit;
        this.laParcelle  = laParcelle;
    }
    
    public abstract Float   quantiteAppliquee();
    
    
    public abstract void afficher();
    
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public String getIdTrait() {
        return idTrait;
    }
    
    public void setIdTrait(String idTrait) {
        this.idTrait = idTrait;
    }
    
    public Produit getLeProduit() {
        return leProduit;
    }
    
    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }
    
    public Parcelle getLaParcelle() {
        return laParcelle;
    }
    
    public void setLaParcelle(Parcelle laParcelle) {
        this.laParcelle = laParcelle;
    }
    //</editor-fold>
}


